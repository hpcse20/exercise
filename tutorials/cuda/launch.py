#!/usr/bin/env python3

import sys

def generate(n):
    return '\n'.join([
        '__global__ void dummyKernel2(double x) {',
        '    double x0 = 1.0;',
        *(f'    double x{k+1} = x{k} * x;' for k in range(n)),
        '    double y = 0.0;',
        *(f'    y += x{k+1};' for k in reversed(range(n))),
        '    if (y == 0.1)',
        '        printf("Hello x=%f\\n", x);',
        '}',
    ])

print(generate(int(sys.argv[1])))
