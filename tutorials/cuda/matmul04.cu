// Instruction-level parallelism. Each thread now computes 4 different values.
#include "utils.h"

using real = double;


__global__ void matMulKernel(int N, const real *a, const real *b, real *c) {
    int ix = blockIdx.x * blockDim.x + threadIdx.x;
    int iy = blockIdx.y * blockDim.y + threadIdx.y;

    if (ix >= N || iy >= N)
        return;

    real sum0 = 0;
    real sum1 = 0;
    real sum2 = 0;
    real sum3 = 0;
    for (int k = 0; k < N; ++k) {
        sum0 += a[(4 * iy + 0) * N + k] * b[k * N + ix];
        sum1 += a[(4 * iy + 1) * N + k] * b[k * N + ix];
        sum2 += a[(4 * iy + 2) * N + k] * b[k * N + ix];
        sum3 += a[(4 * iy + 3) * N + k] * b[k * N + ix];
        __syncthreads();
    }
    c[(4 * iy + 0) * N + ix] = sum0;
    c[(4 * iy + 1) * N + ix] = sum1;
    c[(4 * iy + 2) * N + ix] = sum2;
    c[(4 * iy + 3) * N + ix] = sum3;
}

// c = a * b
void matMulCPU(int N, const real *a, const real *b, real *c) {
    for (int iy = 0; iy < N; ++iy)
    for (int ix = 0; ix < N; ++ix) {
        real sum = 0;
        for (int k = 0; k < N; ++k)
            sum += a[iy * N + k] * b[k * N + ix];
        c[iy * N + ix] = sum;
    }
}

void benchmarkMatrixMultiplication(int N, bool check) {
    real *aHost;
    real *bHost;
    real *cHost;
    real *aDev;
    real *bDev;
    real *cDev;

    // Allocate.
    CUDA_CHECK(cudaMallocHost(&aHost, N * N * sizeof(real)));
    CUDA_CHECK(cudaMallocHost(&bHost, N * N * sizeof(real)));
    CUDA_CHECK(cudaMallocHost(&cHost, N * N * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&aDev, N * N * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&bDev, N * N * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&cDev, N * N * sizeof(real)));

    // Prepare A and B.
    for (int iy = 0; iy < N; ++iy)
    for (int ix = 0; ix < N; ++ix) {
        aHost[iy * N + ix] = iy + ix;
        bHost[iy * N + ix] = ix * ix + iy;
    }
    CUDA_CHECK(cudaMemcpy(aDev, aHost, N * N * sizeof(real), cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemcpy(bDev, bHost, N * N * sizeof(real), cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemset(cDev, 0, N * N * sizeof(real)));

    // Compute C = A * B on GPU.
    double dt = benchmark(10, [N, aDev, bDev, cDev]() {
        dim3 threads(32, 32, 1);
        dim3 blocks((N + threads.x - 1) / threads.x,
                    (N + threads.y * 4 - 1) / (threads.y * 4),
                    1);
        matMulKernel<<<blocks, threads>>>(N, aDev, bDev, cDev);
    });
    double gflops = 1e-9 * 2LL * N * N * N / dt;
    printf("N=%d   GFLOP/s=%.1f\n", N, gflops);

    // Check correctnes.
    if (check) {
        matMulCPU(N, aHost, bHost, cHost);
        double *tmpHost = aHost;
        CUDA_CHECK(cudaMemcpy(tmpHost, cDev, N * N * sizeof(real), cudaMemcpyDeviceToHost));
        for (int iy = 0; iy < N; ++iy)
        for (int ix = 0; ix < N; ++ix) {
            if (tmpHost[iy * N + ix] != cHost[iy * N + ix]) {
                fprintf(stderr, "Incorrect result at [%d][%d] --> host=%f gpu=%f\n",
                        iy, ix, cHost[iy * N + ix], tmpHost[iy * N + ix]);
                exit(1);
            }
        }
        // printf("GPU result correct.\n");
    }

    // Deallocate.
    CUDA_CHECK(cudaFree(cDev));
    CUDA_CHECK(cudaFree(bDev));
    CUDA_CHECK(cudaFree(aDev));
    CUDA_CHECK(cudaFreeHost(cHost));
    CUDA_CHECK(cudaFreeHost(bHost));
    CUDA_CHECK(cudaFreeHost(aHost));
}


int main() {
    benchmarkMatrixMultiplication(256, true);
    benchmarkMatrixMultiplication(259, true);
    benchmarkMatrixMultiplication(3072, false);
}
